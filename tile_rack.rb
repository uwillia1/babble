# Represents the current set of seven tiles from which the
# player can make a word.
class TileRack < TileGroup

	# Constructor. Makes a new TileRack.
	def initialize
		super
	end
	
	# Returns the number of tiles needed to bring the rack
	# back up to seven.
	def number_of_tiles_needed
		7 - @tiles.length
	end
	
	# Returns true if the rack has enough tiles to make
	# the word represented by the text parameter.
	def has_tiles_for?(text)
		text = text.upcase
		result = true
		if @tiles.length >= text.length
			text_letters = text.chars
			text_letters.each do
				|text_letter|
				unless @tiles.join.include? text_letter
					result = false
				end
			end
		else
			result = false
		end
		result
	end

	# Returns a Word object made by removing the tiles given
	# by text.
	def remove_word(text)
		text = text.upcase
		word = Word.new
		if has_tiles_for? text
			text_array = text.split("")
			text_array.each do
				|letter|
				letter = letter.to_sym
				@tiles.delete_at(@tiles.index(letter))
				word.append(letter)
			end
		end
		word
	end
	
end