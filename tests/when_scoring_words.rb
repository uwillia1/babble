# run with:
# ruby -Ilib:test when_scoring_words.rb

require "minitest/autorun"
require_relative "../word.rb"

# Tests words for correct scores
class WhenScoringWords < Minitest::Test

	# Creates objects for tests
	def setup
		@word = Word.new
	end
	
	# Empty word has score of zero
	def test_empty_word_should_have_score_of_zero
		assert_equal 0, @word.score
	end

	# Q = 10
	def test_score_a_one_tile_word
		@word.append(:Q)
		assert_equal 10, @word.score
	end
	
	# Q = 10; U = 1; A = 1; C = 3; K = 5; QUACK = 20
	def test_score_a_word_with_multiple_different_tiles
		@word.append(:Q)
		@word.append(:U)
		@word.append(:A)
		@word.append(:C)
		@word.append(:K)
		assert_equal 20, @word.score
	end
	
	# QQQ = 30
	def test_score_a_word_with_recurring_tiles
		@word.append(:Q)
		@word.append(:Q)
		@word.append(:Q)
		assert_equal 30, @word.score
	end
	
end