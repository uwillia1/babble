# run with:
# ruby -Ilib:test when_removing_words_from_tile_racks.rb

require "minitest/autorun"
require_relative "../tile_rack.rb"

# Tests TileRack#remove_word
class WhenRemovingWordsFromTileRacks < Minitest::Test

	# Makes objects for testing
	def setup
		@rack = TileRack.new
	end
	
	# Remove word whose letters are on the rack
	def test_can_remove_a_word_whose_letters_are_in_order_on_the_rack
		@rack.append :C
		@rack.append :A
		@rack.append :T
		assert_equal("CAT", @rack.remove_word("CAT").to_s)
	end
	
	# Remove word whose letters are on the rack, but not in order
	def test_can_remove_a_word_whose_letters_are_not_in_order_on_the_rack
		@rack.append :A
		@rack.append :C
		@rack.append :T
		assert_equal("CAT", @rack.remove_word("CAT").to_s)
	end
	
	# Remove word with duplicate letters
	def test_can_remove_word_with_duplicate_letters
		@rack.append :C
		@rack.append :C
		@rack.append :T
		assert_equal("CCT", @rack.remove_word("CCT").to_s)
	end
	
	# Remove word without removing duplicate tiles from the rack
	def test_can_remove_word_without_removing_unneeded_duplicate_letters
		@rack.append :C
		@rack.append :C
		@rack.append :A
		@rack.append :T
		assert_equal("CAT", @rack.remove_word("CAT").to_s)
	end

end