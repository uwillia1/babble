# run with:
# ruby -Ilib:test when_converting_tile_group_to_string.rb

require "minitest/autorun"
require_relative "../tile_group.rb"

# Tests TileGroup::to_s
class WhenCovertingTileGroupToString < Minitest::Test

	# Creates objects for tests
	def setup
		@tile_group = TileGroup.new
	end

	# Empty tile group has empty string
	def test_convert_empty_group_to_string
		assert_equal "", @tile_group.to_s
	end

	# Tile group with single character has string with same single character
	def test_convert_single_tile_group_to_string
		@tile_group.append(:A)
		assert_equal "A", @tile_group.to_s
	end

	# Tile group with word yields string of same word
	def test_convert_multi_tile_group_to_string
		@tile_group.append(:U)
		@tile_group.append(:L)
		@tile_group.append(:V)
		@tile_group.append(:E)
		@tile_group.append(:R)
		assert_equal "ULVER", @tile_group.to_s
	end

	# Tile group with repeating character yields string of same repeating character
	def test_convert_multi_tile_group_with_repeating_values_to_string
		@tile_group.append(:B)
		@tile_group.append(:B)
		@tile_group.append(:B)
		assert_equal "BBB", @tile_group.to_s
	end

end
