# run with:
# ruby -Ilib:test when_finding_number_of_tiles_needed.rb

require "minitest/autorun"
require_relative "../tile_rack.rb"

# Tests TileRack::number_of_tiles_needed
class WhenFindingNumberOfTilesNeeded < Minitest::Test

	# Makes objects for testing
	def setup
		@rack = TileRack.new
		@max = 7
	end

	# An empty rack needs 7 tiles
	def test_empty_tile_rack_should_need_max_tiles
		assert_equal @max, @rack.number_of_tiles_needed
	end
	
	# A rack with 1 tile needs 6 tiles
	def test_tile_rack_with_one_tile_should_need_max_minus_one_tiles
		@rack.append :A
		assert_equal @max - 1, @rack.number_of_tiles_needed
	end
	
	# A rack with 3 tiles needs 4 tiles
	def test_tile_rack_with_several_tiles_should_need_some_tiles
		3.times do
			@rack.append :A
		end
		assert_equal @max - 3, @rack.number_of_tiles_needed
	end
	
	# A rack with 7 tiles needs no tiles
	def test_that_full_tile_rack_doesnt_need_any_tiles
		7.times do
			@rack.append :A
		end
		assert_equal 0, @rack.number_of_tiles_needed
	end

end