# run with:
# ruby -Ilib:test when_seeing_if_a_tile_rack_has_the_tiles_needed_to_make_word.rb

require "minitest/autorun"
require_relative "../tile_rack.rb"

# Tests TileRack::has_tiles_for?
class WhenSeeingIfATileRackHasTheTilesNeededToMakeWord < Minitest::Test

	# Makes objects for tests
	def setup
		@rack = TileRack.new
	end

	# Rack has letters for CAT
	def test_rack_has_needed_letters_when_letters_are_in_order_no_duplicates
		@rack.append :C
		@rack.append :A
		@rack.append :T
		assert_equal(true, @rack.has_tiles_for?("CAT"))
	end
	
	# Rack has letters for CAT but not in order
	def test_rack_has_needed_letters_when_letters_are_not_in_order_no_duplicates
		@rack.append :A
		@rack.append :C
		@rack.append :T
		assert_equal(true, @rack.has_tiles_for?("CAT"))
	end
	
	# Rack does not have any needed letters
	def test_rack_doesnt_contain_any_needed_letters
		@rack.append :D
		@rack.append :O
		@rack.append :G
		assert_equal(false, @rack.has_tiles_for?("CAT"))
	end
	
	# Rack has only 2 letters for CAT
	def test_rack_contains_some_but_not_all_needed_letters
		@rack.append :A
		@rack.append :C
		assert_equal(false, @rack.has_tiles_for?("CAT"))
	end
	
	# Rack contains duplicate tiles for a word with duplicate letters
	def test_rack_contains_a_word_with_duplicate_letters
		@rack.append :C
		@rack.append :C
		@rack.append :T
		assert_equal(true, @rack.has_tiles_for?("CCT"))
	end
	
	# Rack does not contain appropriate duplicates
	def test_rack_doesnt_contain_enough_duplicate_letters
		@rack.append :C
		@rack.append :T
		assert_equal(false, @rack.has_tiles_for?("CCT"))
	end

end