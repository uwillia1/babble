# run with:
# ruby -Ilib:test when_drawing_tiles.rb

require "minitest/autorun"
require_relative "../tile_bag.rb"

# Unit tests for TileBag#draw_tile method
class WhenDrawingTiles < Minitest::Test

	# Creates objects for tests
	def setup
		@bag = TileBag.new
	end

	# A bag should have 98 tiles
	def test_has_proper_number_of_tiles
		98.times do
			@bag.draw_tile
		end
		assert_equal true, @bag.empty?
	end
	
	# Number of letters should be distributed like they are in Scrabble
	def test_has_proper_tile_distribution
		tiles = @bag.tiles
		assert_equal 9, tiles.count(:A)
		assert_equal 2, tiles.count(:B)
		assert_equal 2, tiles.count(:C)
		assert_equal 4, tiles.count(:D)
		assert_equal 12, tiles.count(:E)
		assert_equal 2, tiles.count(:F)
		assert_equal 3, tiles.count(:G)
		assert_equal 2, tiles.count(:H)
		assert_equal 9, tiles.count(:I)
		assert_equal 1, tiles.count(:J)
		assert_equal 1, tiles.count(:K)
		assert_equal 4, tiles.count(:L)
		assert_equal 2, tiles.count(:M)
		assert_equal 6, tiles.count(:N)
		assert_equal 8, tiles.count(:O)
		assert_equal 2, tiles.count(:P)
		assert_equal 1, tiles.count(:Q)
		assert_equal 6, tiles.count(:R)
		assert_equal 4, tiles.count(:S)
		assert_equal 6, tiles.count(:T)
		assert_equal 4, tiles.count(:U)
		assert_equal 2, tiles.count(:V)
		assert_equal 2, tiles.count(:W)
		assert_equal 1, tiles.count(:X)
		assert_equal 2, tiles.count(:Y)
		assert_equal 1, tiles.count(:Z)
	end

end