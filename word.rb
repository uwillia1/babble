# A Word is a meaningfull collection of tiles
class Word < TileGroup
	
	# Creates a new Word
	def initialize
		super
		@points = { A: 1, B: 3, C: 3, D: 2, E: 2, F: 4, G: 2, H: 4, I: 1, J: 8, K: 5, L: 1, M: 3, N: 1, O: 1, P: 3, Q: 10, R: 1, S: 1, T: 1, U: 1, V: 4, W: 4, X: 8, Y: 4, Z: 10 }
	end

	# Returns the sum of all points for the tiles in this Word
	def score
		sum = 0
		@tiles.each do 
			|tile|
			sum += @points[tile]
		end
		return sum
	end

end
