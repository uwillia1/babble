
require './tile_group.rb'
require './tile_bag.rb'
require './tile_rack.rb'
require './word.rb'
require 'spellchecker'
require 'tempfile'

# Driver class for Babble program.
class Babble

	# Zero param constructor for making Babble objects
	def initialize
		@bag = TileBag.new
		@rack = TileRack.new
		7.times do
			@rack.append(@bag.draw_tile)
		end
		#@spellchecker = Spellchecker.new
	end
	
	# Main program loop
	def run
		points = 0
		total_points = 0
		quit = false
		rack_string = "Rack: " + @rack.to_s
		until quit do
			puts
			puts rack_string
			print "Guess a word > "
			input = gets.chomp
			if input.eql? ":quit"
				puts "Thank for playing, total score: " + total_points.to_s + " points"
				quit = true
			elsif Spellchecker::check(input) [0] [:correct]
				if @rack.has_tiles_for? input
					word = @rack.remove_word input
					points = word.score
					total_points += points
					puts "You made " + word.to_s + " for " + points.to_s + " points"
					puts "Current score: " + total_points.to_s + " points"
					@rack.number_of_tiles_needed.times do
						@rack.append(@bag.draw_tile)
					end
					rack_string = "Rack: " + @rack.to_s
				else
					puts "Not enough tiles"
					puts "Current score: " + total_points.to_s + " points"
				end
			else
				puts "Not a valid word"
				puts "Current score: " + total_points.to_s + " points"
			end
		end
	end

end

# Starts a new game of Babble.
Babble.new.run