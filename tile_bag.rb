# A bag of letter tiles
class TileBag
	
	# Constructor for TileBag.
	def initialize
		@bag = Array.new
		12.times do
			@bag << :E
		end
		9.times do
			@bag << :A
			@bag << :I
		end
		8.times do
			@bag << :O
		end
		6.times do
			@bag << :N
			@bag << :R
			@bag << :T
		end
		4.times do
			@bag << :D
			@bag << :L
			@bag << :S
			@bag << :U
		end
		3.times do
			@bag << :G
		end
		2.times do
			@bag << :B
			@bag << :C
			@bag << :F
			@bag << :H
			@bag << :M
			@bag << :P
			@bag << :V
			@bag << :W
			@bag << :Y
		end
		@bag << :J
		@bag << :K
		@bag << :Q
		@bag << :X
		@bag << :Z
		
	end
	
	# Returns a random tile from the TileBag
	def draw_tile
		@bag.delete_at(rand(@bag.length))
	end
	
	# Tells if the bag is empty.
	# Returns true if empty, false otherwise
	def empty?
		@bag.empty?
	end
	
	# Returns the point value for the specified tile
	def self.points_for(tile)
		points = { 
			A: 1, 
			B: 3, 
			C: 3, 
			D: 2, 
			E: 2, 
			F: 4, 
			G: 2, 
			H: 4, 
			I: 1, 
			J: 8, 
			K: 5, 
			L: 1, 
			M: 3, 
			N: 1, 
			O: 1, 
			P: 3, 
			Q: 10, 
			R: 1, 
			S: 1, 
			T: 1, 
			U: 1, 
			V: 4, 
			W: 4, 
			X: 8, 
			Y: 4, 
			Z: 10 
		}
		points[tile]
	end
	
	# Returns the collections of tiles in the TileBag
	def tiles
		@bag
	end
	
end