# Parent of TileRack and Word classes
class TileGroup

	# Creates a new TileGroup
	def initialize
		@tiles = Array.new
	end

	# Appends a tile to the group
	def append(tile)
		@tiles.push(tile)
	end

	# Removes the specified tile from the group
	def remove(tile)
		index = @tiles.find_index(tile)
		@tiles.delete_at(index)
	end

	# Returns a concatination of all the tiles' string values
	def to_s
		tile_strings = ""
		@tiles.each do
			|tile|
			tile_strings += tile.to_s
		end
		return tile_strings
	end

end
